----------------------------------------------------------------------------------
INTRODUCTION
----------------------------------------------------------------------------------

Cari works with plugins. A plugin is required for every software package. When a
plugin is added then the latest (or any other available version) of that software
package can be installed.

Default supported plugins (from the Cari Gitlab plugin repo) can be added by 
simply using "cari add <name>". Other unsupported/custom plugins can be added by 
passing a Gitlab or Github repo URL.

(Note: plugins use the verbs "add" and "remove", whereas packages use "install" 
and "uninstall". A plugin and package have the same <name>.)

Example ~ getting Julia:
> cari add julia                # Adds the plugin
> cari install julia latest     # Installs latest version of package
> cari exec julia               # Run the command/executable
> cari which julia              # Get the dir. path to the command

Cari can install and manage any number of package-versions, but unless you must
have different versions of a package, or if uncertain, the "latest" (ie stable) 
version is the best approach. But be aware of which version of a package is 
being executed - `cari current` can help with this.

See below for all cari commands and options.

----------------------------------------------------------------------------------
MANAGE PLUGINS - A plugin is required before installing software/packages
----------------------------------------------------------------------------------

cari add <name> [<git-url>]             - Add a plugin from the default plugin 
                                          repo OR a specific Git repo
cari remove <name>                      - Warning! Removes a plugin and all its 
                                          packages +versions
cari plugins                            - List official plugins on the cari-plugins
                                          repo (* indicates added)
cari plugins --added                    - List all added plugins   
                                  
cari update <name>                      - Update a specific (added) plugin

cari update                             - Update all (added) plugins

----------------------------------------------------------------------------------
MANAGE PACKAGES - Install and manage software package specified by a plugin
----------------------------------------------------------------------------------

cari list <name> [<filter>]             - List all available versions of a package
                                          with optional version-filter
cari list                               - List all installed packages +versions

cari list --installed                   - (As above)

cari install <name>                     - Install the latest stable version of a
                                          package
cari install <name> latest[:<filter>]   - Install the latest stable version that
                                          begins with optional version-filter
cari install <name> <version>           - Install specific version of a package
                                          (stable or otherwise)
cari uninstall <name> <version>         - Uninstall specific version of a package

cari which <command>                    - Display the path to an executable

cari global <name> <version>            - Set the package global version

cari global <name> latest[:<version>]   - Set the package global version to the
                                          latest (stable) given version
cari shell <name> <version>             - Set the package version to
                                          CARI_<PKG>_VERSION in current shell
cari current <name>                     - Display currently set version of a 
                                          package (global or shell context)
cari help <name> [<version>]            - Output documentation for plugin and tool

cari exec <command> [args...]           - Executes the command shim for current 
                                          version

----------------------------------------------------------------------------------
CARI UTILS - Managing the Cari tool itself
----------------------------------------------------------------------------------

cari [--help | -h]                      - Show the help (this) screen

cari upgrade                            - Update cari to the latest stable 
                                          release
cari upgrade --head                     - Update cari to the latest on the main 
                                          branch
cari --version                          - Show the current version of cari 
                                         
cari info                               - Print OS, Shell and CARI system 
                                          information
cari env <name> [util]                  - Runs util (default: `env`) inside the
                                          environment used for command shim 
                                          execution                                          
cari reshim <name> <version>            - Recreate shims for version of a package

cari shim-versions <name>               - List the plugins and versions that
                                          provide a command
