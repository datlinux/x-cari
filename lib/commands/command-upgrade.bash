# -*- sh -*-

upgrade_command() {
  local update_to_head=$1

  (
    cd "$(cari_dir)" || exit 1

    if [ -f cari_updates_disabled ] || ! git rev-parse --is-inside-work-tree &>/dev/null; then
      printf "Update command disabled. Please use the package manager that you used to install cari to upgrade cari.\\n"
      exit 42
    else
      do_upgrade "$update_to_head"
    fi
  )
}

do_upgrade() {

  local update_to_head=$1
  
  if [ "$update_to_head" = "--head" ]; then
    # Update to latest on the main branch
    git fetch -q origin main
    git checkout -q main
    git reset -q --hard origin/main
    printf "Updated cari to latest on the main branch\\n"
  else
    # Update to latest release
    git fetch -q -t origin main || exit 1

    if [ "$(get_cari_config_value "use_release_candidates")" = "yes" ]; then
      # Use the latest tag whether or not it is an RC
      tag=$(git tag | sort_versions | sed '$!d') || exit 1
    else
      # Exclude RC tags when selecting latest tag
      tag=$(git tag | sort_versions | grep -vi "rc" | sed '$!d') || exit 1
    fi

    # Update
    git checkout -q  "$tag" || exit 1
    printf "Updated cari to release %s\\n" "$tag"
  fi
  
}

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    LC_ALL=C sort -t. -k 1,1 -k 2,2n -k 3,3n -k 4,4n -k 5,5n | awk '{print $2}'
}

upgrade_command "$@"

