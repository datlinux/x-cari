# -*- sh -*-

remove_command() {
  local plugin_name=$1
  check_if_plugin_exists "$plugin_name"

  local plugin_path
  plugin_path=$(get_plugin_path "$plugin_name")

  cari_run_hook "pre_cari_plugin_remove" "$plugin_name"
  cari_run_hook "pre_cari_plugin_remove_${plugin_name}"

  if [ -f "${plugin_path}/bin/pre-plugin-remove" ]; then
    (
      export CARI_PLUGIN_PATH=$plugin_path
      "${plugin_path}/bin/pre-plugin-remove"
    )
  fi

  rm -rf "$plugin_path"
  rm -rf "$(cari_data_dir)/installs/${plugin_name}"
  rm -rf "$(cari_data_dir)/downloads/${plugin_name}"

  grep -l "cari-plugin: ${plugin_name}" "$(cari_data_dir)"/shims/* 2>/dev/null | xargs rm -f

  cari_run_hook "post_cari_plugin_remove" "$plugin_name"
  cari_run_hook "post_cari_plugin_remove_${plugin_name}"
}

remove_command "$@"















